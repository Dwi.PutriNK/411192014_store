@extends('template')

@section('content')
<div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                <div class="card-body">
                        <h2>Data Penjualan</h2>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('penjualan.create') }}" class="btn btn-md btn-success mb-3">TAMBAH BARANG</a>
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">No.</th>
                                <th scope="col">No Penjualan</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Kode Pelanggan</th>
                                <th scope="col">Nama Pelanggan</th>
                                <th scope="col">Kode Barang Barang</th>
                                <th scope="col">Nama Barang</th>
                                <th scope="col">Jumlah Barang</th>
                                <th scope="col">harga Barang</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @forelse ($penjualan as $item)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $item->no_penjualan }}</td>
                                    <td>{{ $item->tanggal }}</td>
                                    <td>{{ $item->kode_pelanggan }}</td>
                                    <td>{{ $item->nama_pelanggan }}</td>
                                    <td>{{ $item->kode_barang }}</td>
                                    <td>{{ $item->nama_barang }}</td>
                                    <td>{{ $item->jumlah_barang }}</td>
                                    <td>{{ $item->harga_barang }}</td>
                                    <td class="text-center">
                                            <a href="{{ route('penjualan.show', $item->id) }}" class="btn btn-info btn-sm"><span 
                                            class="material-symbols-outlined">info</span></a>
                                            @csrf
                                    </td>
                                </tr>
                              @empty
                                  <div class="alert alert-danger">
                                      Data Blog belum Tersedia.
                                  </div>
                              @endforelse
                            </tbody>
                          </table>  
                          {{ $penjualan->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        //message with toastr
        @if(session()->has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>
   <!--{!! $penjualan->links() !!} -->
</html>

@endsection