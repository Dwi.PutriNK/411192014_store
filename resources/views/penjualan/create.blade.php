@extends('template')

@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create New Penjualan</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('penjualan.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Input gagal.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('penjualan.store') }}" method="POST">
    @csrf

     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>No Penjualan :</strong>
                <input type="text" maxlength="15" name="no_penjualan" class="form-control" placeholder="PJ0000000000001">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tanggal:</strong>
                <input type="date" name="tanggal" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Kode Pelanggan:</strong>
                    <select name="kode_pelanggan" class="form-control @error('kode_pelanggan') is-invalid @enderror">
                         <option value="">-- PILIH --</option>
                          @foreach ($pelanggan as $item)
                            <option value="{{ $item->kode_pelanggan }}" {{ old('kode_pelanggan') == $item->id ? 'selected' : null }}>
                              {{ $item->kode_pelanggan }}</option>
                        @endforeach
                    </select>
                </div>        
        </div>        
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nama Pelanggan:</strong>
                    <select name="nama_pelanggan" class="form-control @error('nama_pelanggan') is-invalid @enderror">
                         <option value="">-- PILIH --</option>
                          @foreach ($pelanggan as $item)
                            <option value="{{ $item->nama_pelanggan }}" {{ old('nama_pelanggan') == $item->id ? 'selected' : null }}>
                              {{ $item->nama_pelanggan }}</option>
                      @endforeach
                    </select>
                </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Kode Barang:</strong>
                    <select name="kode_barang" class="form-control @error('nama_barang') is-invalid @enderror">
                          <option value="">-- PILIH --</option>
                          @foreach ($barang as $item)
                            <option value="{{ $item->kode_barang }}" {{ old('kode_barang') == $item->id ? 'selected' : null }}>
                              {{ $item->kode_barang }}</option>
                      @endforeach
                    </select>
                </div>
        </div> 
        <div class="col-xs-12 col-sm-12 col-md-12">
               <div class="form-group">
                    <strong>Nama Barang:</strong>
                    <select name="nama_barang" class="form-control @error('nama_barang') is-invalid @enderror">
                         <option value="">-- PILIH --</option>
                         @foreach ($barang as $item)
                           <option value="{{ $item->nama_barang }}" {{ old('nama_barang') == $item->id ? 'selected' : null }}>
                              {{ $item->nama_barang }}</option>
                      @endforeach
                    </select>
                </div>
        </div>       
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jumlah Barang:</strong>
                <input type="number" maxlength="99" name="jumlah_barang" class="form-control" placeholder="MAX INPUT 99"  >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Harga Barang:</strong>
                <input type="number" maxlength="99" name="harga_barang" class="form-control" placeholder="MAX INPUT 99"  >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection