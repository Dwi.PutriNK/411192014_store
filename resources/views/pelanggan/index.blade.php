@extends('template')

@section('content')
<div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                <div class="card-body float-left">
                        <h2>Data Pelanggan</h2>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('pelanggan.create') }}" class="btn btn-md btn-success mb-3">TAMBAH BARANG</a>
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Kode Pelanggan</th>
                                <th scope="col">Nama Pelanggan</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">Nama Kota</th>
                                <th scope="col">No Telepone</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @forelse ($pelanggan as $item)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $item->kode_pelanggan }}</td>
                                    <td>{{ $item->nama_pelanggan }}</td>
                                    <td>{{ $item->alamat }}</td>
                                    <td>{{ $item->nama_kota }}</td>
                                    <td>{{ $item->no_telepon }}</td>
                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" 
                                        action="{{ route('pelanggan.destroy', $item->id) }}" method="POST">
                                            
                                            <a href="{{ route('pelanggan.show', $item->id) }}" 
                                            class="btn btn-info btn-sm"><span class="material-symbols-outlined">info</span></a>
                                            
                                            <a href="{{ route('pelanggan.edit', $item->id) }}" 
                                            class="btn btn-sm btn-primary"><span class="material-symbols-outlined">edit</span></a>
                                            
                                            @csrf
                                            @method('DELETE')
                                           
                                            <button type="submit" class="btn btn-sm btn-danger"><span 
                                            class="material-symbols-outlined">delete</span></button>
                                        </form>
                                    </td>
                                </tr>
                              @empty
                                  <div class="alert alert-danger">
                                      Data Blog belum Tersedia.
                                  </div>
                              @endforelse
                            </tbody>
                          </table>  
                          {{ $pelanggan->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        //message with toastr
        @if(session()->has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>
   <!--{!! $pelanggan->links() !!} -->
</html>

@endsection